package com.dictionary.tiufanov.denis.dictionary

import android.database.Cursor
import android.databinding.DataBindingUtil
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.KeyEvent
import android.view.View

import com.dictionary.tiufanov.denis.dictionary.dataBase.DictionaryObject
import com.dictionary.tiufanov.denis.dictionary.dataBase.OperationsDataBaseHelper

import java.util.Random

import android.view.View.*
import com.dictionary.tiufanov.denis.dictionary.databinding.ActivityCheckKnowledgeBinding
import org.jetbrains.anko.sdk15.listeners.onClick
import org.jetbrains.anko.toast

//COMPONENT VIEW
class CheckKnowledgeActivity : AppCompatActivity() {

    var repository = OperationsDataBaseHelper

    lateinit var binding: ActivityCheckKnowledgeBinding

    private lateinit var dictionaryObject: DictionaryObject

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_check_knowledge)
        val oldestElementsSearching = object : OperationsDataBaseHelper.OldestElementsSearchCallback {
            override fun onOldestElementSearched(cursor: Cursor) {
                cursor.moveToFirst()
                fillFields(cursor)
                buttonsInitialisation()
                binding.textResult.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        if (dictionaryObject.studiedText
                                .equals(binding.textResult.text.toString(), ignoreCase = true)) {
                            repository.updateRepeatedTime(applicationContext,
                                    dictionaryObject)
                        }
                        return@OnKeyListener true
                    }
                    false
                })
                binding.textResult.onClick { binding.textResult.setText("") }
            }
        }
        repository.getOldestElements(applicationContext, oldestElementsSearching)
    }

    private fun fillFields(cursor: Cursor) {
        val numberElements = cursor.count
        if (numberElements <= 1) {
            return
        }
        val position = Random().nextInt(numberElements - 1)
        cursor.moveToPosition(position)
        val oldestDictionaryObject = repository.getDictionaryObjectFromCursor(cursor)
        val minTimeElementSearchCallback = object : OperationsDataBaseHelper.MinTimeElementSearchedCallback {
            override fun onMinTimeElementSearched(dictionaryObject: DictionaryObject) {
                this@CheckKnowledgeActivity.dictionaryObject = dictionaryObject
                if (TextUtils.isEmpty(dictionaryObject.nativeText)) {
                    binding.textOnSourceLanguage.text = getString(R.string.all_words_repeated)
                } else {
                    binding.textOnSourceLanguage.text = dictionaryObject.nativeText
                }
            }
        }
        val minUpdateTime = java.lang.Long.valueOf(oldestDictionaryObject.repeatedTime)
        repository.getDictionaryObjectByMinTime(
                applicationContext, minUpdateTime, minTimeElementSearchCallback)
    }

    private fun buttonsInitialisation() {
        binding.checkButton.setOnClickListener(View.OnClickListener { v ->
            if (TextUtils.isEmpty(binding.textResult.text)) {
                toast("Please enter your answer!")
                return@OnClickListener
            }
            if (dictionaryObject.studiedText.equals(binding.textResult.text.toString(),
                    ignoreCase = true)) {
                binding.rightTranslation.setBackgroundColor(Color.GREEN)
                repository.updateRepeatedTime(applicationContext, dictionaryObject)
            } else {
                binding.rightTranslation.setBackgroundColor(Color.RED)
            }
            binding.rightTranslation.visibility = VISIBLE
            binding.rightTranslation.text = dictionaryObject.studiedText
            v.visibility = INVISIBLE
            binding.nextButton.visibility = VISIBLE
        })
        binding.skipButton.onClick { closeActivity("action canceled") }
        binding.nextButton.setOnClickListener { v ->
            binding.textResult.setText("")
            binding.rightTranslation.visibility = INVISIBLE
            v.visibility = INVISIBLE
            binding.checkButton.visibility = VISIBLE
            val searchingOldestElements = object : OperationsDataBaseHelper.OldestElementsSearchCallback {
                override fun onOldestElementSearched(cursor: Cursor) {
                    fillFields(cursor)
                }
            }
            repository.getOldestElements(applicationContext, searchingOldestElements)
        }
    }

    private fun closeActivity(sendText: String) {
        onBackPressed()
        toast(sendText)
    }
}
