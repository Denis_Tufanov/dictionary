package com.dictionary.tiufanov.denis.dictionary

import android.content.Intent
import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.dictionary.tiufanov.denis.dictionary.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.sdk15.listeners.onClick

//COMPONENT VIEW
class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        binding.apply {
            addWords.onClick {
                val addNewWordsIntent = Intent(applicationContext, NewDataInDictionaryActivity::class.java)
                startActivity(addNewWordsIntent)
            }
            checkKnowledge.onClick {
                val checkKnowledgeIntent = Intent(applicationContext, CheckKnowledgeActivity::class.java)
                startActivity(checkKnowledgeIntent)
            }
        }
    }
}
