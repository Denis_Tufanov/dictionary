package com.dictionary.tiufanov.denis.dictionary.dataBase

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.DatabaseErrorHandler
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns
import android.util.Log

import java.util.ArrayList

//MODEL
class OperationsDataBaseHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null,
        DATABASE_VERSION), BaseColumns {

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(DATABASE_CREATE_SCRIPT)
    }

    override fun onUpgrade(db: SQLiteDatabase,
                           oldVersion: Int,
                           newVersion: Int) {

        Log.w("SQLite", "Update from version $oldVersion to version $newVersion")

        // Remove old table
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE)
        // Create new table
        onCreate(db)
    }

    companion object {

        private val DATABASE_NAME = "database.db"

        private val DATABASE_VERSION = 1

        val DATABASE_TABLE = "dictionary"

        val TYPE_COLUMN = "type"
        val NATIVE_COLUMN = "native_language"
        val STUDIED_COLUMN = "studied_language"
        val TIME_COLUMN = "repeated_time" // in milleseconds
        // Create database
        private val DATABASE_CREATE_SCRIPT = "create table " + DATABASE_TABLE + " (" + BaseColumns._ID + " integer primary key autoincrement, " + TYPE_COLUMN + " string, " + NATIVE_COLUMN + " text not null, " + STUDIED_COLUMN + " text not null, " + TIME_COLUMN + " long);"
        // Select All Query
        private val selectAllQuery = "SELECT " + "*" + " FROM " + OperationsDataBaseHelper.DATABASE_TABLE

        fun deleteDBElement(context: Context,
                            dictionaryObject: DictionaryObject,
                            databaseElementDeleted: DatabaseDeleteCallback) {
            val mDatabaseHelper = OperationsDataBaseHelper(context)
            val db = mDatabaseHelper.writableDatabase

            val whereClause = StringBuilder()
            whereClause.append(OperationsDataBaseHelper.TYPE_COLUMN + "=?" + " and ")
            whereClause.append(OperationsDataBaseHelper.NATIVE_COLUMN + "=?" + " and ")
            whereClause.append(OperationsDataBaseHelper.STUDIED_COLUMN + "=?" + " and ")
            whereClause.append(OperationsDataBaseHelper.TIME_COLUMN + "=?")
            val whereArgs = arrayOf(dictionaryObject.type, dictionaryObject.nativeText,
                    dictionaryObject.studiedText, dictionaryObject.repeatedTime)
            db.beginTransaction()
            db.delete(OperationsDataBaseHelper.DATABASE_TABLE, whereClause.toString(), whereArgs)
            db.endTransaction()
            databaseElementDeleted.onDataElementDeletion(checkIfRawDeleted(context, dictionaryObject))

            db.close()
            mDatabaseHelper.close()
        }

        private fun checkIfRawDeleted(context: Context,
                                      dictionaryObject: DictionaryObject): Boolean {
            // Select All Query
            val selectQuery = "SELECT " + "*" + " FROM " + OperationsDataBaseHelper.DATABASE_TABLE

            val db = OperationsDataBaseHelper(context).readableDatabase
            val cursor = db.rawQuery(selectQuery, null)

            // looping through all rows and adding to list
            var detected = false
            if (cursor.moveToFirst()) {
                do {
                    Log.d("raw " + cursor.count,
                            TYPE_COLUMN + "=" + cursor.getString(cursor.getColumnIndex(OperationsDataBaseHelper.TYPE_COLUMN)) + ", "
                                    + NATIVE_COLUMN + "="
                                    + cursor.getString(cursor.getColumnIndex(OperationsDataBaseHelper.NATIVE_COLUMN)) + ", "
                                    + STUDIED_COLUMN + "="
                                    + cursor.getString(cursor.getColumnIndex(OperationsDataBaseHelper.STUDIED_COLUMN)) + ", "
                                    + TIME_COLUMN + "="
                                    + cursor.getString(cursor.getColumnIndex(OperationsDataBaseHelper.TIME_COLUMN)))
                    if (dictionaryObject.type.equals(cursor
                            .getString(cursor.getColumnIndex(OperationsDataBaseHelper.TYPE_COLUMN)), ignoreCase = true)
                            && dictionaryObject.nativeText.equals(cursor
                            .getString(cursor.getColumnIndex(OperationsDataBaseHelper.NATIVE_COLUMN)), ignoreCase = true)
                            && dictionaryObject.studiedText.equals(cursor
                            .getString(cursor.getColumnIndex(OperationsDataBaseHelper.STUDIED_COLUMN)), ignoreCase = true)
                            && dictionaryObject.repeatedTime.equals(cursor.getString(cursor
                            .getColumnIndex(OperationsDataBaseHelper.TIME_COLUMN)), ignoreCase = true)) {
                        Log.d("SQLDataBase", "detected raw deleting")
                        detected = true
                    }
                } while (cursor.moveToNext())
            }
            // closing connection
            cursor.close()
            db.close()
            return detected
        }

        fun insertDBElement(context: Context,
                            dictionaryObject: DictionaryObject,
                            databaseElementInserted: DatabaseInsertCallback) {
            val databaseSelector = object : DatabaseSelectorCallback {
                override fun onDataElementsSelected(selectedElements: List<DictionaryObject>) {
                    if (selectedElements.contains(dictionaryObject)) {
                        updateStudiedWord(context, dictionaryObject)
                        databaseElementInserted.onDataElementInsertion(false)
                    } else {
                        val mDatabaseHelper = OperationsDataBaseHelper(context)
                        val db = mDatabaseHelper.writableDatabase
                        val values = ContentValues()
                        values.put(OperationsDataBaseHelper.TYPE_COLUMN, dictionaryObject.type)
                        values.put(OperationsDataBaseHelper.NATIVE_COLUMN, dictionaryObject.nativeText)
                        values.put(OperationsDataBaseHelper.STUDIED_COLUMN, dictionaryObject.studiedText)
                        values.put(OperationsDataBaseHelper.TIME_COLUMN, dictionaryObject.repeatedTime)
                        db.insert(OperationsDataBaseHelper.DATABASE_TABLE, null, values)
                        databaseElementInserted.onDataElementInsertion(true)
                        db.close()
                    }
                }
            }
            getElementsList(context, databaseSelector)
        }

        fun getElementsList(context: Context, collectionElementsCompleted: DatabaseSelectorCallback) {
            val operationObjectList = ArrayList<DictionaryObject>()

            val db = OperationsDataBaseHelper(context).readableDatabase
            db.beginTransaction()
            val cursor = db.rawQuery(selectAllQuery, null)
            if (cursor.moveToFirst()) {
                do {
                    operationObjectList.add(getDictionaryObjectFromCursor(cursor))
                } while (cursor.moveToNext())
            }
            db.endTransaction()
            db.close()
            collectionElementsCompleted.onDataElementsSelected(operationObjectList)
        }

        fun getDictionaryObjectFromCursor(cursor: Cursor): DictionaryObject {
            return DictionaryObject.Builder(
                    cursor.getString(cursor
                            .getColumnIndex(OperationsDataBaseHelper.TYPE_COLUMN)),
                    cursor.getString(cursor
                            .getColumnIndex(OperationsDataBaseHelper.NATIVE_COLUMN)),
                    cursor.getString(cursor
                            .getColumnIndex(OperationsDataBaseHelper.STUDIED_COLUMN)),
                    cursor.getString(cursor
                            .getColumnIndex(OperationsDataBaseHelper.TIME_COLUMN)))
                    .build()
        }

        fun getOldestElements(context: Context,
                              searchingOldestElements: OldestElementsSearchCallback) {
            val db = OperationsDataBaseHelper(context).readableDatabase
            val cursor = db.rawQuery("SELECT * " + " FROM " + OperationsDataBaseHelper.DATABASE_TABLE
                    + " ORDER BY " + OperationsDataBaseHelper.TIME_COLUMN + " LIMIT 5", null)
            searchingOldestElements.onOldestElementSearched(cursor)
            db.close()
        }

        fun getDictionaryObjectByMinTime(context: Context,
                                         updatedTime: Long,
                                         searchingByMinTime: MinTimeElementSearchedCallback) {
            val projection = arrayOf(OperationsDataBaseHelper.TIME_COLUMN,
                    OperationsDataBaseHelper.TYPE_COLUMN, OperationsDataBaseHelper.NATIVE_COLUMN,
                    OperationsDataBaseHelper.STUDIED_COLUMN)

            val selection = OperationsDataBaseHelper.TIME_COLUMN + " = ?"
            val selectionArgs = arrayOf(updatedTime.toString())
            val db = OperationsDataBaseHelper(context).readableDatabase

            val cursor = db.query(
                    OperationsDataBaseHelper.DATABASE_TABLE, // The table to query
                    projection, // The columns to return
                    selection, // The columns for the WHERE clause
                    selectionArgs, null, null, null
            )// The values for the WHERE clause
            // don't group the rows
            cursor.moveToFirst()
            searchingByMinTime.onMinTimeElementSearched(getDictionaryObjectFromCursor(cursor))
            db.close()
        }

        fun updateRepeatedTime(context: Context,
                               updateThisObject: DictionaryObject): Boolean {

            val db = OperationsDataBaseHelper(context).writableDatabase
            val sql = "update " + DATABASE_TABLE + " set " + TIME_COLUMN + "='" + System.currentTimeMillis().toString() + "'" + " where " + TIME_COLUMN + " like ?"
            val bindArgs = arrayOf<Any>(updateThisObject.repeatedTime)
            try {
                db.execSQL(sql, bindArgs)
                db.close()
                return true
            } catch (ex: SQLException) {
                Log.d("DataBase: ", "update data failure")
                db.close()
                return false
            }

        }

        fun updateStudiedWord(context: Context,
                              updateThisObject: DictionaryObject): Boolean {

            val db = OperationsDataBaseHelper(context).writableDatabase
            val sql = "update " + DATABASE_TABLE + " set " + STUDIED_COLUMN + "='" + updateThisObject.studiedText + "'" + " where " + NATIVE_COLUMN + " like ?"
            val bindArgs = arrayOf<Any>(updateThisObject.nativeText)
            try {
                db.execSQL(sql, bindArgs)
                return true
            } catch (ex: SQLException) {
                Log.d("DataBase: ", "update studied word failure")
                return false
            }

        }
    }

    interface MinTimeElementSearchedCallback {
        fun onMinTimeElementSearched(dictionaryObject: DictionaryObject)
    }

    interface OldestElementsSearchCallback {
        fun onOldestElementSearched(cursor: Cursor)
    }

    interface DatabaseSelectorCallback {
        fun onDataElementsSelected(selectedElements: List<DictionaryObject>)
    }

    interface DatabaseInsertCallback {
        fun onDataElementInsertion(completed: Boolean)
    }

    interface DatabaseDeleteCallback {
        fun onDataElementDeletion(completed: Boolean)
    }
}
