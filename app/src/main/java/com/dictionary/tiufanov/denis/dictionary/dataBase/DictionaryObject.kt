package com.dictionary.tiufanov.denis.dictionary.dataBase

class DictionaryObject private constructor(val type: String,
                                           val nativeText: String,
                                           val studiedText: String,
                                           val repeatedTime: String) {

    class Builder(private var type: String,
                  private var nativeText: String,
                  private var studiedText: String,
                  private var repeatedTime: String) {
        fun build(): DictionaryObject {
            return DictionaryObject(type, nativeText, studiedText, repeatedTime)
        }
    }
}
