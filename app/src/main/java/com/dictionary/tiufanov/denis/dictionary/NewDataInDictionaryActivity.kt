package com.dictionary.tiufanov.denis.dictionary

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils

import com.dictionary.tiufanov.denis.dictionary.dataBase.DictionaryObject
import com.dictionary.tiufanov.denis.dictionary.dataBase.OperationsDataBaseHelper
import com.dictionary.tiufanov.denis.dictionary.databinding.ActivityNewDataInDictionaryBinding
import org.jetbrains.anko.sdk15.listeners.onClick
import org.jetbrains.anko.toast

//COMPONENT VIEW
class NewDataInDictionaryActivity : AppCompatActivity() {

    var repository = OperationsDataBaseHelper

    lateinit var binding: ActivityNewDataInDictionaryBinding

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_new_data_in_dictionary)
        buttonsInitialisation()
    }

    private fun buttonsInitialisation() {
        binding.enterAction.onClick {
            if (TextUtils.isEmpty(binding.nativeLanguageInput.text.toString())) {
                toast("Fill native field please")
                return@onClick
            }
            if (TextUtils.isEmpty(binding.studiedLanguageInput.text.toString())) {
                toast("Fill studied field please")
                return@onClick
            }
            val insertionResult = object : OperationsDataBaseHelper.DatabaseInsertCallback {
                override fun onDataElementInsertion(completed: Boolean) {
                    closeActivity(if (completed) {
                        "dictionary updated"
                    } else {
                        "update error"
                    })
                }
            }
            val savedObject = DictionaryObject.Builder("String",
                    binding.nativeLanguageInput.text.toString(),
                    binding.studiedLanguageInput.text.toString(),
                    System.currentTimeMillis().toString()).build()
            repository.insertDBElement(applicationContext, savedObject, insertionResult)
        }
        binding.cancelAction.onClick { closeActivity("action canceled") }
    }

    private fun closeActivity(sendText: String) {
        onBackPressed()
        toast(sendText)
    }
}
